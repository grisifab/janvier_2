package exo3;

import java.util.Scanner;

public class Exo3 {

	public static void main(String[] args) {
		// calculatrice
		
				int num1, num2, resultat;
				String s1;
				
				Scanner monScanner = new Scanner(System.in);
				
				do {
				
				System.out.println("Rentrer l'op�ration � r�aliser X +(ou /-*) Y ");
				num1 = monScanner.nextInt();
				s1 = monScanner.next();
				num2 = monScanner.nextInt();
				monScanner.nextLine(); //pour vider le scanner
				
				
				switch (s1) {
				case "+" :
					resultat = addition(num1,num2);
					break;
				case "-" :
					resultat = soustraction(num1,num2);
					break;
				case "*" :
					resultat = multiplication(num1,num2);
					break;
				case "/" :
					resultat = division(num1,num2);
					break;
				default :
					resultat = 0;
				}				
				System.out.println(resultat);
				
				} while (!(s1.contains("end")));
				
				monScanner.close();
	}
	
	public static int addition(int a, int b) {
		return(a + b);
	}

	public static int soustraction(int a, int b) {
		return(a - b);
	}
	
	public static int multiplication(int a, int b) {
		return(a * b);
	}
	
	public static int division(int a, int b) {
		if (b != 0) {
			return(a / b);
		} else {
			return(0);
		}	
	}
}

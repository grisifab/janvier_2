package exo10;

public class Exo10 {

	public static void main(String[] args) {
		// �crivez un programme Java pour trouver les plus petits 
		// et les deuxi�mes plus petits �l�ments d'un tableau donn� 
		
		int[] tabIn = {5,7,-8,5,14,1};
		int[] tabOut = new int[tabIn.length];
		final int maxInt = 2147483647;
		int minActu;
		int ind = 0;
		
		for(int i = 0; i < tabOut.length; i++){
			minActu = maxInt;
			for(int j = 0; j < tabIn.length; j++){
				if (tabIn[j] < minActu){
					ind = j;
					minActu = tabIn[j];
				}
			}
			tabOut[i] = minActu;
			tabIn[ind] = maxInt;
		}
		
		System.out.println("Le plus petit est : " + tabOut[0]);
		System.out.println("Le second plus petit est : " + tabOut[1]);
	}
}

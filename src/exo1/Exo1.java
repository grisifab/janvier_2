package exo1;

public class Exo1 {

	public static void main(String[] args) {
		// Sans m�thode, on tape n fois la m�me chose (en commentaire maintenant)
		String prenom_1 = "Titi";
		String prenom_2 = "Toto";
		String prenom_3 = "Tata";
		String[] prenoms = {"Titi","Toto","Tata"};
		
		int age_1 = 8;
		int age_2 = 16;
		int age_3 = 24;
		int[] ages = {8,16,24};
		
		ageCondition(prenom_1, age_1);
		ageCondition(prenom_2, age_2);
		ageCondition(prenom_3, age_3);
		
		System.out.println(prenom_1 + " est un " + ageConditionBis(age_1));
		System.out.println(prenom_2 + " est un " + ageConditionBis(age_2));
		System.out.println(prenom_3 + " est un " + ageConditionBis(age_3));
		
		for(int i = 0; i<prenoms.length; i++) {
			System.out.println(prenoms[i] + " est un " + ageConditionBis(ages[i]));
		}
		
//		if (age_1 < 12) {
//			System.out.println(prenom_1 + " est un enfant");
//		} else if (age_1 < 18) {
//			System.out.println(prenom_1 + " est un ado");
//		} else {
//			System.out.println(prenom_1 + " est un adulte");
//		}
//		
//		if (age_2 < 12) {
//			System.out.println(prenom_2 + " est un enfant");
//		} else if (age_2 < 18) {
//			System.out.println(prenom_2 + " est un ado");
//		} else {
//			System.out.println(prenom_2 + " est un adulte");
//		}
//		
//		if (age_3 < 12) {
//			System.out.println(prenom_3 + " est un enfant");
//		} else if (age_3 < 18) {
//			System.out.println(prenom_3 + " est un ado");
//		} else {
//			System.out.println(prenom_3 + " est un adulte");
//		}
	}
	
	public static void ageCondition(String prenom, int age) {
		if (age < 12) {
			System.out.println(prenom + " est un enfant");
		} else if (age < 18) {
			System.out.println(prenom + " est un ado");
		} else {
			System.out.println(prenom + " est un adulte");
		}
	}
	
	public static String ageConditionBis(int age) {
		if (age < 12) {
			return "enfant";
		} else if (age < 18) {
			return "ado";
		} else {
			return "adulte";
		}
	}

}

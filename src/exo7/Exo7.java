package exo7;

import java.util.Scanner;

public class Exo7 {

	public static void main(String[] args) {
		// �crivez un programme Java qui accepte quatre entiers de l'utilisateur 
		// et imprime �gal si tous les quatre sont �gaux, et non �gaux sinon.
		
		int[] entree = {0,0,0,0};
		
		Scanner monScanner = new Scanner(System.in);
		
		for (int i = 0; i <4; i++) {
			System.out.println("Saisir Nombre N� " + i);
			entree[i] = monScanner.nextInt();
			monScanner.nextLine(); //pour vider le scanner
		}
		
		if(((entree[0]/entree[1])*(entree[2]/entree[3])*(entree[1]/entree[2])) == 1) {
			System.out.println("Egal");
		} else {
			System.out.println("Non egal");
		}
		
		monScanner.close();

	}

}

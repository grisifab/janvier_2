package exo8;

import java.util.Scanner;

public class Exo8 {

	public static void main(String[] args) {
		// �crivez un programme Java pour afficher la vitesse, 
		// en m�tres par seconde, kilom�tres par heure et miles par heure. 
		// Saisie utilisateur: distance (en m�tres) et le temps a �t� pris 
		// (sous forme de trois chiffres: heures, minutes, secondes). 
		
		double distance, vitesseMs, vitesseKmh, vitesseMph;
		int heure, minute, seconde;
		
		Scanner monScanner = new Scanner(System.in);
		System.out.println("Rentrer la distance en m : ");
		distance = monScanner.nextDouble();
		monScanner.nextLine(); //pour vider le scanner
		System.out.println("Rentrer le temps sous ce format : H M S");
		heure = monScanner.nextInt();
		minute = monScanner.nextInt();
		seconde = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner
		monScanner.close();
		
		seconde += ((minute*60) + (heure*3600));
		vitesseMs = distance / (double) seconde;
		vitesseKmh = vitesseMs * 3.6;
		vitesseMph = vitesseKmh / 1.60934;
		
		System.out.println("La vitesse en m par s est de : " + vitesseMs);
		System.out.println("La vitesse en Km par h est de : " + vitesseKmh);
		System.out.println("La vitesse en miles par s est de : " + vitesseMph);	

	}

}

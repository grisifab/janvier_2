package exo11;

import java.util.Scanner;

public class Exo11 {

	public static void main(String[] args) {
		// Ecrire un programme pour que pour un cours du dollar donn�
		// (et qui ne change pas pendantl�ex�cution du programme),
		// on puisse convertir en euro. Pr�voyez un moyen d�arr�ter l�ex�cution du
		// programme.
		double dollar, euro;
		final double conv = 0.89; // 1 dollar vaut 0.89 euro

		Scanner monScanner = new Scanner(System.in);
		
		do {
			System.out.println("Saisir valeur dollar � convertir ou 0 pour arr�ter : ");
			dollar = monScanner.nextDouble();
			monScanner.nextLine(); // pour vider le scanner
			euro = dollar * conv;
			System.out.println("La valeur en euro est de : " + euro);
		} while (Math.abs(dollar) > 0.0001);

		monScanner.close();
	}

}

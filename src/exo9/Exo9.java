package exo9;

import java.util.Scanner;

public class Exo9 {

	public static void main(String[] args) {
		// �crivez un programme Java qui accepte trois nombres 
		// et affiche "Tous les nombres sont �gaux" si les trois nombres sont �gaux,
		// "Tous les nombres sont diff�rents" si les trois nombres sont diff�rents 
		// et sinon "Ni tous ni �gaux ni diff�rents"
		
		int[] entree = {0,0,0};
		
		Scanner monScanner = new Scanner(System.in);
		
		for (int i = 0; i <3; i++) {
			System.out.println("Saisir Nombre N� " + i);
			entree[i] = monScanner.nextInt();
			monScanner.nextLine(); //pour vider le scanner
		}
		
		if((entree[0] == entree[1]) && (entree[1] == entree[2])) {
			System.out.println("Tous les nombres sont �gaux");
		} else if ((entree[0] != entree[1]) && (entree[1] != entree[2]) && (entree[0] != entree[2])) {
			System.out.println("Tous les nombres sont diff�rents");
		} else {
			System.out.println("Ni tous ni �gaux ni diff�rents");
		}
		
		monScanner.close();
	}

}

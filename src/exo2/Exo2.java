package exo2;

import java.util.Scanner;

public class Exo2 {

	public static void main(String[] args) {
		// user rentre un N� de mois on lui dit la saison
		int num1;
		
		Scanner monScanner = new Scanner(System.in);
		num1 = monScanner.nextInt();
		monScanner.nextLine(); //pour vider le scanner
		monScanner.close();
		
		System.out.println(moisSaison(num1));

	}
	
	public static String moisSaison(int num) {
		num--;
		num = (num >= 0) ? num : -12;
		num /=3;
		
		switch (num) {
		case 0:
			return("Hiver");
		case 1:
			return("Primtemps");
		case 2:
			return("Et�");
		case 3:
			return("Automne");
		default:
			return("mauvaise saisie");
		}
	}

}
